import './App.css';
import Container from '@mui/material/Container';
import { makeStyles } from '@mui/styles';
import { LoginPage } from "./pages/LoginPage";
import { MainPage } from './pages/MainPage';
import { Routes, Route, Navigate } from "react-router-dom";

const useStyles = makeStyles({
	root: {
		background: "#f0f0f0",
		height: "100%",
	},
});

function App() {
	const classes = useStyles();
	return (
		<Container maxWidth="xl" container="main" className={classes.root}>
			{/* <LoginPage /> */}
			{/* <MainPage /> */}
			<Routes>
				<Route exact path="/" element={<Navigate replace to="/login" />} />
				<Route path="/login" element={<LoginPage />} />
				{/* <Route path="/register" element={<LoginPage />} /> */}
				<Route path="/currencies" element={<MainPage />} />
			</Routes>

		</Container>
	);
}

export default App;
