import React, { useState, useEffect } from "react";

import Container from '@mui/material/Container';
import { Box } from "@mui/material";
import { makeStyles } from '@mui/styles';
import Typography from '@mui/material/Typography';
import { PageHeader } from "../components/PageHeader";
import BasicTable from "../components/CurrencyTable";
import { BASE_URL } from "../common/constants.js";
import { WatchTable } from "../components/WatchTable";


const useStyles = makeStyles({
	root: {
		background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
		border: 0,
		borderRadius: 3,
		boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
		color: 'white',
		height: "100%",
	},
	table: {
		marginTop: "50px",
	},
	watchTable: {
		margin: "20px auto"
	}
});


export const MainPage = () => {
	const [currencies, setCurrencies] = useState([]);
	const classes = useStyles();

	const getCurrencies = async () => {
		const res = sessionStorage.getItem("accessToken");

		const response = await fetch(`${BASE_URL}currencies`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `${res}`
			},
		});
		const info = await response.json();
		// console.log("INFO", info);
		setCurrencies([...info]);
	};


	useEffect(() => {
		getCurrencies();
	}, []);
	return (
		<Container maxWidth="xl"
			sx={{

				height: "100%",
			}}
		>
			<Box className={classes.root}>
				<PageHeader />
				<BasicTable
					currencies={currencies}
					className={classes.table}
				/>
				<div className={classes.watchTable}>
					<Typography variant="h6" sx={{ textAlign: "center" }}>Отслеживаемые валюты</Typography>
					<WatchTable />
				</div>
			</Box>

		</Container>

	);
};