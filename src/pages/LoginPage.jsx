import React from "react";
import { useNavigate } from "react-router-dom";
import Container from '@mui/material/Container';
import { Box } from "@mui/material";
import Button from '@mui/material/Button';
import { makeStyles } from '@mui/styles';
import Typography from '@mui/material/Typography';
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { Form } from "../components/Form";
import { FormInput } from "../components/FormInput";
import { BASE_URL } from "../common/constants.js";

/*Схема проверки полей*/
const scheme = yup.object().shape({
	username: yup
		.string()
		.matches(/^([0-9a-zA-Z]*)$/, "Разрешены только цифры и буквы")
		.max(20, "Максимум 20 символов")
		.required("Обязательное поле"),
	password: yup
		.string()
		.matches(/^([0-9a-zA-Z]*)$/, "Разрешены только цифры и буквы")
		.max(20, "Максимум 20 символов")
		.required("Обязательное поле"),
});

const useStyles = makeStyles({
	root: {
		background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
		border: 0,
		borderRadius: 3,
		boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
		color: 'white',
		height: "100%",
		// padding: '0 30px',
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
	},
	formBody: {
		// backgroundColor: "#FFF",

	}
});

export const LoginPage = () => {
	const classes = useStyles();
	const navigate = useNavigate();

	const {

		handleSubmit,
		control,
		formState: { errors },
	} = useForm({
		mode: "onBlur", //Режим валидации(по событию)
		resolver: yupResolver(scheme), //Запуск внешнего метода проверки
	});

	const onSubmit = async (data) => {

		console.log("DATA", data);
		const response = await fetch(`${BASE_URL}login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				username: data.username,
				password: data.password,

			}),
		});
		const token = await response.json();
		sessionStorage.setItem("accessToken", token.Authorization);
		sessionStorage.setItem("currentUser", data.username);


		navigate("/currencies");
	};

	return (
		<Container maxWidth="xl"
			sx={{

				height: "100%",
			}}
		>

			<Box className={classes.root}>
				<Box>
					<Typography variant="h6" sx={{ textAlign: "center" }}>Вход в профиль</Typography>
					<Box className={classes.formBody}>
						<Form onSubmit={handleSubmit(onSubmit)}>
							<FormInput
								// {...register("username")}
								control={control}
								id="username"
								type="text"
								label="Имя"
								name="username"
								error={errors}
							/>
							<FormInput
								control={control}
								id="password"
								type="text"
								label="Пароль"
								name="password"
								error={errors}
							/>
							<Button
								variant="contained"
								type="submit"
								color="success"
								fullWidth
								sx={{ marginTop: "20px" }}
							>
								Войти
							</Button>
						</Form>
					</Box>
				</Box>
			</Box>
		</Container>
	);
};