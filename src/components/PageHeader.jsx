import React from "react";
import { useNavigate } from "react-router-dom";
import { Box } from "@mui/material";
import { makeStyles } from '@mui/styles';
import LogoutIcon from '@mui/icons-material/Logout';
import Avatar from '@mui/material/Avatar';
import { deepPurple } from '@mui/material/colors';



const useStyles = makeStyles({
	header: {
		height: "70px",
		// backgroundColor: "#fff",
		borderBottom: "1px solid #fff",
		display: "flex",
		justifyContent: "flex-end",
		alignItems: "center",
		paddingRight: "20px"
	},
	icon: {
		marginLeft: "20px",
		'&:hover': {
			cursor: "pointer"
		}
	},
	userinfo: {
		marginLeft: "10px",
		verticalAlign: "center"
	}
});



export const PageHeader = () => {
	const classes = useStyles();
	const navigate = useNavigate();
	const userName = sessionStorage.getItem("currentUser");

	const upperName = userName.charAt(0).toUpperCase() + userName.slice(1);
	const firstLetter = upperName.charAt(0)


	const logout = () => {
		console.log("logout");
		sessionStorage.clear();
		navigate("/login");
	};

	return (
		<Box className={classes.header}>
			<Avatar sx={{ bgcolor: deepPurple[500] }}>{firstLetter}</Avatar>
			<div

				className={classes.userinfo}
			>
				{userName}
			</div>
			<LogoutIcon
				className={classes.icon}
				onClick={logout}
			/>
		</Box>
	);
};