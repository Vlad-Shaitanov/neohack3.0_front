import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { makeStyles } from '@mui/styles';
import Button from '@mui/material/Button';
import { BASE_URL } from '../common/constants';


const useStyles = makeStyles({
	row: {
		'&:hover': {
			cursor: "pointer",
			backgroundColor: "#f0f0f0"
		}
	},

});

export default function BasicTable(props) {
	const { currencies } = props;
	const classes = useStyles();

	const res = sessionStorage.getItem("accessToken");

	const handleSubscr = async (row) => {

		const response = await fetch(`${BASE_URL}notification/subscribe`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `${res}`
			},
			body: JSON.stringify({
				currency: `${row.figi}`

			}),
		});
	};

	const handleUnSubscr = async (row) => {

		const response = await fetch(`${BASE_URL}notification/unsubscribe`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				Authorization: `${res}`
			},
			body: JSON.stringify({
				currency: `${row.figi}`

			}),
		});
	};
	console.log("CURRENCIES", currencies);



	// const getFormatDate = (timestamp) => {
	// 	const date = new Date(timestamp);
	// 	const newDate = `${date.getDate()}/${date.getMonth() + 1
	// 		}/${date.getFullYear()}`;
	// 	const stringDate = newDate.toString();
	// };


	return (
		<TableContainer component={Paper} sx={{ margin: "30px auto", maxWidth: "90%" }} elevation={3}>
			<Table aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell>Currency</TableCell>
						<TableCell align="right">Last Price</TableCell>
						<TableCell align="right">Last Update</TableCell>
						<TableCell align="right"></TableCell>
						<TableCell align="right"></TableCell>
					</TableRow>
				</TableHead>
				<TableBody >
					{currencies.map((row, idx) => (
						<TableRow
							className={classes.row}
							key={row.figi + idx}
							sx={{
								'&:last-child td, &:last-child th': { border: 0 },
								".css-177gid-MuiTableCell-root": {
									padding: "6px"
								}
							}}
						>
							<TableCell component="th" scope="row">{row.figi}</TableCell>
							<TableCell align="right">${row.lastPrice}</TableCell>
							<TableCell align="right">{row.updateDate}</TableCell>
							<TableCell align="right">
								<Button
									onClick={() => handleSubscr(row)}
									variant="contained"
									color="success"
									fullWidth
									sx={{ marginTop: "20px" }}
								>
									Следить
								</Button>
							</TableCell>
							<TableCell align="right">
								<Button
									onClick={() => handleUnSubscr(row)}
									variant="contained"
									color="warning"
									fullWidth
									sx={{ marginTop: "20px" }}
								>
									Отписка
								</Button>
							</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
}