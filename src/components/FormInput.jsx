import React from "react";
import TextField from "@mui/material/TextField";
import { Controller } from "react-hook-form";
import FormHelperText from "@mui/material/FormHelperText";

export const FormInput = ({ control, name, error, ...inputProps }) => {
	return (
		<Controller
			control={control}
			name={name}
			{...inputProps}
			defaultValue=""
			render={({
				field: { onChange, onBlur, value, name, ref },
				// fieldState: { invalid, isTouched, isDirty, error },
				// formState,
			}) => (
				<>
					<TextField
						variant="filled"
						margin="normal"
						fullWidth
						onChange={onChange}
						value={value}
						error={!!error[name]}
						{...inputProps}
					/>
					<FormHelperText error>{error?.[name]?.message}</FormHelperText>
				</>
			)}
		/>
	);
};