import React, { useEffect, useState } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { BASE_URL } from '../common/constants';

export const WatchTable = () => {
	const [watchCurrs, setWatchCurrs] = useState([]);

	const res = sessionStorage.getItem("accessToken");

	const getWatchableCurrencies = async () => {

		const response = await fetch(`${BASE_URL}notification`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `${res}`
			},

		});
		const watchableCurr = await response.json();
		const data = watchableCurr.candleDetailsList;


		setWatchCurrs([...data]);
		// console.log("WATCH", watchableCurr.candleDetailsList);
		console.log("watchCurrs", watchCurrs);
		console.log("WATCHABLE", data);
	};

	useEffect(() => {
		let timerId = setInterval(() => getWatchableCurrencies(), 5000);
		// getWatchableCurrencies();

		return () => {
			clearInterval(timerId);
		};
	}, [watchCurrs]);

	return (
		<TableContainer component={Paper} sx={{ margin: "30px auto", maxWidth: "90%" }} elevation={3}>
			<Table aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell>Currency</TableCell>
						<TableCell align="right">Low</TableCell>
						<TableCell align="right">High</TableCell>
						<TableCell align="right">Open</TableCell>
						<TableCell align="right">Close</TableCell>
						<TableCell align="right">Open Time</TableCell>
					</TableRow>
				</TableHead>
				<TableBody >
					{watchCurrs.map((row, idx) => (
						<TableRow

							key={row.low + idx}
							sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
						>
							<TableCell component="th" scope="row">{row.currency}</TableCell>
							<TableCell align="right">${row.low}</TableCell>
							<TableCell align="right">${row.high}</TableCell>
							<TableCell align="right">{row.open}</TableCell>
							<TableCell align="right">{row.close}</TableCell>
							<TableCell align="right">{row.openTime}</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};